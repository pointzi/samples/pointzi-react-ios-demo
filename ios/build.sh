#!/bin/sh -x
echo "Build started on $(date)"
set -e

# ---------------------- build and upload to hockeyapp using fastlane---------------------
cd ios
fastlane beta
